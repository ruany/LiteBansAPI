package litebans.api;

import litebans.api.exception.MissingImplementationException;
import lombok.Setter;

public abstract class RandomID {
    public static final int RESULT_ERROR = -1;
    @Setter
    private static RandomID instance;

    public static RandomID get() {
        if (instance == null) throw new MissingImplementationException();
        return instance;
    }

    /**
     * Converts a number into a randomized ID. Input must be a positive integer value.
     * @param id the number
     * @return random ID
     */
    public abstract String convert(long id);

    /**
     * Converts a randomized ID to the number it represents.
     * @param randomID the randomized ID
     * @return a number, or RESULT_ERROR if the input string is invalid
     */
    public abstract long reveal(String randomID);
}
