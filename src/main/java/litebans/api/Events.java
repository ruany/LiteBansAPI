package litebans.api;

import litebans.api.exception.MissingImplementationException;
import lombok.Setter;
import org.jetbrains.annotations.*;

/**
 * Created by ruan on 2017-04-17 at 07:53.
 * <p>
 * This class allows you to listen to events.
 * All events are fired async. If you want to run code on the main thread, schedule a task.
 * <p>
 * Limitations of current implementation:
 * - entryAdded and entryRemoved events will not be fired across multiple plugin instances. This may change in the future, but it will require systems to be redesigned, since the plugin currently doesn't synchronize unneeded data. Broadcast events are synchronized as long as broadcast synchronization is enabled.
 * - Events are not cancellable, they are fired post-execution.
 * - Wildcard unbans (including /staffrollback) will not fire events for every single removed entry. These entries are removed at once using a single database query and are never processed by the plugin (which is efficient).
 */
public abstract class Events {
    @Setter
    private static Events instance;

    public static Events get() {
        if (instance == null) throw new MissingImplementationException();
        return instance;
    }

    public abstract void register(Listener listener);

    public abstract void unregister(Listener listener);

    public static class Listener {
        /**
         * Called after a broadcast or notification is sent to all players who have permission to see it.
         *
         * @param message the broadcasted message
         * @param type    the type of broadcast, if not null, "litebans.notify.[type]" permission is required to see the broadcast
         */
        public void broadcastSent(@NotNull String message, @Nullable String type) {}

        /**
         * Called after an entry (ban, mute, warning, kick) is added to the database.
         */
        public void entryAdded(Entry entry) {}

        /**
         * Called after an entry (ban, mute, warning, kick) is removed from the database.
         */
        public void entryRemoved(Entry entry) {}
    }
}
