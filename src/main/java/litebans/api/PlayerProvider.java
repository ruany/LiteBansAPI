package litebans.api;

import litebans.api.exception.MissingImplementationException;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

/**
 * Created by ruan on 2020-04-24 at 19:44.
 * This class allows you to override the targeted player argument in commands.
 */
public abstract class PlayerProvider {
    @Setter
    private static PlayerProvider instance;

    public static PlayerProvider get() {
        if (instance == null) throw new MissingImplementationException();
        return instance;
    }

    /**
     * This method is always called asynchronously.
     * The returned player does not need to be online.
     * Return the original target if you want to leave it unmodified.
     *
     * @param target The originally targeted player name
     * @return The new target, name or UUID or IP, not null.
     */
    @NotNull
    public abstract String provide(String target);
}
