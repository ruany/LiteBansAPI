package litebans.api;

import litebans.api.exception.MissingImplementationException;
import lombok.NonNull;
import lombok.Setter;
import org.jetbrains.annotations.*;

import java.sql.*;
import java.util.*;

/**
 * Created by ruan on 2017-03-23 at 09:22.
 * <p>
 * This class allows for database queries to be executed on players.
 * All of the methods offered by this class are implemented by database queries, as all of this data is stored in the database, and not in memory.
 * Queries are completely thread-safe and can be executed concurrently.
 * Performing too many concurrent queries may exhaust the connection pool (default max 10 connections).
 * Under Bukkit, queries will throw an IllegalStateException if executed on the main server thread.
 */
public abstract class Database {
    public static final String ANY_SERVER_SCOPE = "__ALL__";

    @Setter
    private static Database instance;

    public static Database get() {
        if (instance == null) throw new MissingImplementationException();
        return instance;
    }

    /**
     * @param uuid UUID of player, can be null
     * @param ip   IP address of player, can be null
     * @return true if there is an active ban matching `uuid` or `ip` on the default server scope, false otherwise
     */
    public abstract boolean isPlayerBanned(@Nullable UUID uuid, @Nullable String ip);

    /**
     * @param uuid   UUID of player, can be null
     * @param ip     IP address of player, can be null
     * @param server Server scope to check, can be null to only check global scope. Specify Database.ANY_SERVER_SCOPE to check all server scopes.
     * @return true if there is an active ban matching `uuid` or `ip`, false otherwise
     */
    public abstract boolean isPlayerBanned(@Nullable UUID uuid, @Nullable String ip, @Nullable String server);

    /**
     * @param uuid UUID of player, can be null
     * @param ip   IP address of player, can be null
     * @return true if there is an active mute matching `uuid` or `ip` on the default server scope, false otherwise
     */
    public abstract boolean isPlayerMuted(@Nullable UUID uuid, @Nullable String ip);

    /**
     * @param uuid   UUID of player, can be null
     * @param ip     IP address of player, can be null
     * @param server Server scope to check, can be null to only check global scope. Specify Database.ANY_SERVER_SCOPE to check all server scopes.
     * @return true if there is an active mute matching `uuid` or `ip`, false otherwise
     */
    public abstract boolean isPlayerMuted(@Nullable UUID uuid, @Nullable String ip, @Nullable String server);

    /**
     * @param id     Ban ID
     * @param server Server scope to check, can be null to only check global scope. Specify Database.ANY_SERVER_SCOPE to check all server scopes.
     * @return An active ban. If there are no active bans matching the given ID, null will be returned.
     */
    @Nullable
    public abstract Entry getBan(long id, @Nullable String server);

    /**
     * @param uuid   UUID of player, can be null
     * @param ip     IP address of player, can be null
     * @param server Server scope to check, can be null to only check global scope. Specify Database.ANY_SERVER_SCOPE to check all server scopes.
     * @return An active ban. If there are no active entries matching the given UUID or IP, null will be returned.
     */
    @Nullable
    public abstract Entry getBan(@Nullable UUID uuid, @Nullable String ip, @Nullable String server);

    /**
     * @param id     Mute ID
     * @param server Server scope to check, can be null to only check global scope. Specify Database.ANY_SERVER_SCOPE to check all server scopes.
     * @return An active mute. If there are no active entries matching the given ID, null will be returned.
     */
    @Nullable
    public abstract Entry getMute(long id, @Nullable String server);

    /**
     * @param uuid   UUID of player, can be null
     * @param ip     IP address of player, can be null
     * @param server Server scope to check, can be null to only check global scope. Specify Database.ANY_SERVER_SCOPE to check all server scopes.
     * @return An active mute. If there are no active entries matching the given UUID or IP, null will be returned.
     */
    @Nullable
    public abstract Entry getMute(@Nullable UUID uuid, @Nullable String ip, @Nullable String server);

    /**
     * @param id     Warn ID
     * @param server Server scope to check, can be null to only check global scope. Specify Database.ANY_SERVER_SCOPE to check all server scopes.
     * @return An active warning. If there are no active entries matching the given ID, null will be returned.
     */
    @Nullable
    public abstract Entry getWarning(long id, @Nullable String server);

    /**
     * @param uuid   UUID of player, can be null
     * @param ip     IP address of player, can be null
     * @param server Server scope to check, can be null to only check global scope. Specify Database.ANY_SERVER_SCOPE to check all server scopes.
     * @return An active warning. If there are no active entries matching the given UUID or IP, null will be returned.
     */
    @Nullable
    public abstract Entry getWarning(@Nullable UUID uuid, @Nullable String ip, @Nullable String server);

    /**
     * @param uuid   UUID of player, can be null
     * @param ip     IP address of player, can be null
     * @param server Server scope to check, can be null to only check global scope. Specify Database.ANY_SERVER_SCOPE to check all server scopes.
     * @return An active kick. If there are no active entries matching the given UUID or IP, null will be returned.
     */
    @Nullable
    public abstract Entry getKick(@Nullable UUID uuid, @Nullable String ip, @Nullable String server);

    /**
     * @param id     Kick ID
     * @param server Server scope to check, can be null to only check global scope. Specify Database.ANY_SERVER_SCOPE to check all server scopes.
     * @return An active kick. If there are no active entries matching the given ID, null will be returned.
     */
    @Nullable
    public abstract Entry getKick(long id, @Nullable String server);

    /**
     * @param ip IP address to scan for accounts
     * @return A collection of UUIDs which are linked to the IP address
     */
    public abstract Collection<UUID> getUsersByIP(String ip);

    /**
     *
     * @param uuid Player UUID
     * @return The last recorded name for the specified account; null if player has never joined the server before.
     */
    @Nullable
    public abstract String getPlayerName(@NonNull UUID uuid);

    /**
     * Example:
     * <p>
     * try (PreparedStatement st = Database.get().prepareStatement("SELECT * FROM {bans}")) {
     * <p>
     * try (ResultSet rs = st.executeQuery()) {
     * <p>
     * ...
     * <p>
     * }
     * <p>
     * }
     *
     * @param sql SQL query. Tokens {bans}, {mutes}, {warnings}, {kicks}, {history}, {servers} will be replaced with real table names including the configured table prefix.
     */
    public abstract PreparedStatement prepareStatement(@NotNull String sql) throws SQLException;
}
