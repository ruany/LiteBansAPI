package litebans.api;

import lombok.*;
import org.jetbrains.annotations.Nullable;

/**
 * Created by ruan on 2015-01-23 at 20:36.
 * This class represents an entry in the database (ban, mute, warning, kick).
 */
@RequiredArgsConstructor
@Getter
@Setter
@ToString
public abstract class Entry {
    /**
     * The ID of this entry.
     */
    private final long id;
    /**
     * The type of this entry.
     * type = {ban,mute,warn,kick}
     */
    private final String type;

    /**
     * The UUID of the player affected by this entry.
     * Nullable. If it is null, only the IP address is affected.
     */
    @Nullable
    private final String uuid;

    /**
     * The IP address of the player affected by this entry. This can be a wildcard specification, e.g. "127.0.0.%"
     * Nullable. If it is null, only the UUID is affected.
     */
    @Nullable
    private final String ip;
    /**
     * The reason for this punishment. Arbitrary string.
     */
    private final String reason;

    /**
     * The UUID of the executor (creator) of this entry.
     */
    @Nullable
    private final String executorUUID;
    /**
     * The display name of the executor (creator) of this entry.
     */
    @Nullable
    private final String executorName;

    /**
     * The UUID of the executor who removed this entry.
     * Nullable. If it is null, this entry probably has not been removed.
     */
    @Nullable
    private final String removedByUUID;
    /**
     * The display name of the executor who removed this entry.
     * Nullable. If it is null, this entry probably has not been removed.
     */
    @Nullable
    private final String removedByName;
    /**
     * The reason for this punishment's removal. Arbitrary string.
     */
    @Nullable
    private final String removalReason;

    /**
     * Date of entry creation (milliseconds unixtime)
     */
    private final long dateStart;
    /**
     * Date of entry expiration (milliseconds unixtime)
     * If dateEnd <= 0, this entry will never expire (use Entry#isPermanent() to check that).
     */
    private final long dateEnd;

    /**
     * The server scope of this entry. (The network, server, or subserver which this entry takes effect on)
     */
    private final String serverScope;
    /**
     * The server origin of this entry. (The server or subserver on which this entry was created)
     */
    private final String serverOrigin;

    /**
     * Whether this entry was created using either the "-s" or "-S" command flags.
     * True if silent, false if not silent.
     */
    private final boolean silent;
    /**
     * True if this entry is an IP ban or an IP mute, false if this entry only affects a UUID.
     */
    private final boolean ipban;
    /**
     * True if this entry is active, has not been deactivated or removed and has not expired yet. False otherwise.
     */
    private final boolean active;

    /**
     * Amount of milliseconds this entry lasts for, from placement until expiry.
     * Returns -1 if permanent.
     */
    public abstract long getDuration();

    /**
     * Returns a string representing the amount of time this entry lasts for, from placement until expiry.
     * E.g. "5 days, 20 minutes, 3 seconds"
     * The format of this string is subject to change.
     * It may contain chat colors, newlines, and translated strings, don't try to parse it.
     */
    public abstract String getDurationString();

    /**
     * Amount of milliseconds until this entry expires.
     * Returns -1 if permanent or already expired.
     */
    public abstract long getRemainingDuration(long currentTime);

    /**
     * Returns a string representing the amount of time until this entry expires, from placement until expiry.
     * E.g. "5 days, 20 minutes, 3 seconds"
     * The format of this string is subject to change.
     * It may contain chat colors, newlines, and translated strings, don't try to parse it.
     */
    public abstract String getRemainingDurationString(long currentTime);

    /**
     * Returns the random ID of this entry.
     */
    public abstract String getRandomID();

    /**
     * Returns true if this entry has expired, false otherwise.
     * Expired entries are deactivated, but deactivated entries are not expired.
     */
    public abstract boolean isExpired(long currentTime);

    /**
     * Returns true if this entry does not have an expiration date.
     */
    public abstract boolean isPermanent();
}
