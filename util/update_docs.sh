#!/bin/bash
set -euxo pipefail

cd $(dirname "$0")/..

if [ -d public ]; then rm -r public; fi
mkdir public

unzip target/LiteBansAPI-javadoc.jar -d public