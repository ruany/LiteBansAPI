group = "sample"
version = "1.0-SNAPSHOT"

plugins {
    kotlin("jvm") version "1.7.0"
}

repositories {
    mavenCentral()
    maven("https://jitpack.io")
    maven("https://hub.spigotmc.org/nexus/content/groups/public")
}

dependencies {
    compileOnly("com.gitlab.ruany", "LiteBansAPI", "0.3.5")
    compileOnly("org.spigotmc", "spigot-api", "1.19.2-R0.1-SNAPSHOT")
    compileOnly("net.md-5", "bungeecord-api", "1.19-R0.1-SNAPSHOT")
}