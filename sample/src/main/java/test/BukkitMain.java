package test;

import litebans.api.*;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.*;

import java.sql.*;
import java.util.UUID;

/**
 * Created by ruan on 2017-04-27 at 08:56.
 */
public class BukkitMain extends JavaPlugin implements Listener {
    @Override
    public void onEnable() {
        Events.get().register(new Events.Listener() {
            @Override
            public void broadcastSent(@NotNull String message, @Nullable String type) {
                System.out.println("New broadcast: '" + message + "'");
                /* ... */
            }

            @Override
            public void entryAdded(Entry entry) {
                if (entry.getType().equals("ban")) {
                    System.out.println("New ban: " + entry);
                }
            }
        });

        PlayerProvider.setInstance(new PlayerProvider() {
            @Override
            public @NotNull String provide(String target) {
                getLogger().info("PlayerProvider.provide(\"" + target + "\")");
                return target;
            }
        });

        getServer().getScheduler().runTaskAsynchronously(this, () -> {
            System.out.println(Database.get().isPlayerBanned(UUID.fromString("b9b618bd-671c-303a-9900-ce0b3f6de348"), null));
            System.out.println(Database.get().isPlayerBanned(UUID.fromString("b9b618bd-671c-303a-9900-ce0b3f6de349"), null));
        });

        getServer().getScheduler().runTaskAsynchronously(this, () -> {
            String sql = "SELECT COUNT(*) FROM {warnings} WHERE uuid=?";
            try (PreparedStatement st = Database.get().prepareStatement(sql)) {
                st.setString(1, "5421d526-ef51-374d-b09e-78ed11f4394f");
                try (ResultSet rs = st.executeQuery()) {
                    if (rs.next()) {
                        System.out.println("warnings: " + rs.getLong(1));
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

        getServer().getPluginManager().registerEvents(this, this);
    }
}
